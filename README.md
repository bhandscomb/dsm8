# dsm8
8-bit multi-CPU disassembler (circa 1995)

I created a couple of simple disassemblers a very long time ago, then decided to merge them so they could in theory share some code. Supports Z80, 6502 and 65816.
