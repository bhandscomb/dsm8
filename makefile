# dsm8

CC=gcc
CFLAGS=-I. -Wall -g0 -Os
LIBS=
DEPS = dsm8.h protos.h
OBJ = cpu6502.o cpu65816.o cpuz80.o dsm8.o sym.o
BIN = dsm8

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

$(BIN): $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

.PHONY: clean

clean:
	rm $(BIN) $(OBJ)

