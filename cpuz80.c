/*
#############################################################################
#
#       $VER: cpuz80.c 2.0 (25.07.95)
#
#############################################################################
*/


/*
#############################################################################
#
#       Includes
#
#############################################################################
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dsm8.h"
#include "protos.h"


/*
#############################################################################
#
#       Prototypes
#
#############################################################################
*/

void addtmp (void);
void decodebyte (unsigned char);
void _C0 (unsigned char);
void _C1 (unsigned char);
void _C2 (unsigned char);
void _C0F0 (void);
void _C0F1 (void);
void _C0F2 (void);
void _C0F3 (void);
void _C1F0 (void);
void _C1F1 (void);
void _C1F2 (void);
void _C1F3 (void);
void _C2F1 (void);
void _C2F2 (void);
void _C0F0H0 (void);
void _C0F0H1 (void);
void _C0F0H2 (void);
void _C0F0H3 (void);
void _C0F0H4 (void);
void _C0F0H5 (void);
void _C0F0H6 (void);
void _C0F0H7 (void);
void _C0F3H0 (void);
void _C0F3H1 (void);
void _C0F3H2 (void);
void _C0F3H3 (void);
void _C0F3H4 (void);
void _C0F3H5 (void);
void _C0F3H6 (void);
void _C0F3H7 (void);
void _C2F1H0 (void);
void _C2F1H1 (void);
void _C2F1H2 (void);
void _C2F1H3 (void);
void _C2F1H4 (void);
void _C2F1H5 (void);
void _C2F1H6 (void);
void _C2F1H7 (void);
void _C0F0H1K0 (void);
void _C0F0H1K1 (void);
void _C0F0H3K0 (void);
void _C0F0H3K1 (void);
void _C0F3H1K0 (void);
void _C0F3H1K1 (void);
void _C0F3H5K0 (void);
void _C0F3H5K1 (void);
void _C2F1H2K0 (void);
void _C2F1H2K1 (void);
void _C2F1H3K0 (void);
void _C2F1H3K1 (void);
void _C2F1H5K0 (void);
void _C2F1H5K1 (void);
void _Z80_EMPTY (void);


/*
#############################################################################
#
#       General look-up tables
#
#############################################################################
*/

char *__r[] =
{
  "B","C","D","E","H","L","x","A"
};

char *__s[] =
{
  "BC","DE","y","SP"
};

char *__q[] =
{
  "BC","DE","y","AF"
};

char *__n[] =
{
  "0","1","2","3","4","5","6","7"
};

char *__c[] =
{
  "NZ","Z","NC","C","PO","PE","P","M"
};

char *__x[] =
{
  "ADD  A,","ADC  A,","SUB  ","SBC  A,",
  "AND  ","XOR  ","OR   ","CP   "
};


/*
#############################################################################
#
#       Decoder function tables
#
#############################################################################
*/

void (*_C[]) (unsigned char) = {_C0,_C1,_C2};

void (*_C0F[]) (void) = {_C0F0,_C0F1,_C0F2,_C0F3};
void (*_C1F[]) (void) = {_C1F0,_C1F1,_C1F2,_C1F3};
void (*_C2F[]) (void) = {_Z80_EMPTY,_C2F1,_C2F2,_Z80_EMPTY};

void (*_C0F0H[]) (void) = {_C0F0H0,_C0F0H1,_C0F0H2,_C0F0H3,
                          _C0F0H4,_C0F0H5,_C0F0H6,_C0F0H7
                         };
void (*_C0F3H[]) (void) = {_C0F3H0,_C0F3H1,_C0F3H2,_C0F3H3,
                          _C0F3H4,_C0F3H5,_C0F3H6,_C0F3H7
                         };
void (*_C2F1H[]) (void) = {_C2F1H0,_C2F1H1,_C2F1H2,_C2F1H3,
                          _C2F1H4,_C2F1H5,_C2F1H6,_C2F1H7
                         };

void (*_C0F0H1K[]) (void) = {_C0F0H1K0,_C0F0H1K1};
void (*_C0F0H3K[]) (void) = {_C0F0H3K0,_C0F0H3K1};
void (*_C0F3H1K[]) (void) = {_C0F3H1K0,_C0F3H1K1};
void (*_C0F3H5K[]) (void) = {_C0F3H5K0,_C0F3H5K1};
void (*_C2F1H2K[]) (void) = {_C2F1H2K0,_C2F1H2K1};
void (*_C2F1H3K[]) (void) = {_C2F1H3K0,_C2F1H3K1};
void (*_C2F1H5K[]) (void) = {_C2F1H5K0,_C2F1H5K1};


/*
#############################################################################
#
#       Globals for this module
#
#############################################################################
*/

unsigned char CLASS, INDEX, __F, __G, __H, __J, __K;
int ilen, DISP;
char DIS[40], DISFINAL[80], *DISFINptr, tmp[40];


/*
#############################################################################
#
#       Z80 decoder
#
#############################################################################
*/

void decodez80 (void)
{
  int x, y, addr;
  char *symbol;
  /* reset globals used during decode */
  CLASS = INDEX = ilen = 0;
  DISP = -1;
  DIS[0] = '\0';
  /* address of instruction if required */
  if (opt_addr)
    printf ("%04x  ", filepos & 0xffff);
  /* symbol? */
  if (opt_sym)
  {
    if ( (symbol = getsym (filepos & 0xffff)) )
      printf (symprintfspec, symbol);
    else
      printf (symprintfspec, "");
  }
  /* get first/only byte of instruction */
  x = getbyte ();
  ilen++;
  /* decode */
  decodebyte (x);
  /* decoding can put in placeholders, take care of these */
  DISFINptr = DISFINAL;
  for (x = 0; x < strlen (DIS); x++)
  {
    switch (DIS[x])
    {
      case '@':
        *DISFINptr++ = '+';
        break;
      case 'v':
        /* handle "vv" - two byte literal value */
        if (DIS[x+1] == 'v')
        {
          x++;
          y = getbyte ();
          addr = (getbyte () << 8) + y;
          ilen += 2;
          if (( symbol = getsym (addr)) )
            strcpy (tmp, symbol);
          else
            sprintf (tmp, "%04x", addr);
          addtmp ();
        }
        else
        {
          /* just "v" - one byte literal value */
          y = getbyte ();
          ilen++;
          if ((DIS[x-1] == '@') && (opt_rel))
          {
            if ( (symbol = getsym (filepos + (char)y)) )
            {
              strcpy (tmp, symbol);
              DISFINptr--;
            }
            else
              sprintf (tmp, "%04x", filepos + (char)y);
          }
          else
          {
            if ((DIS[x-1] == '@') && ( y >= 128))
            {
              sprintf (tmp, "%02x", 256 - y);
              *(DISFINptr-1) = '-';
            }
            else sprintf (tmp, "%02x", y);
          }
          addtmp ();
        }
        break;
      case 'y':
        /* y means HL unless prefix says IX or IY */
        switch (INDEX)
        {
          case 0:
            *DISFINptr++ = 'H';
            *DISFINptr++ = 'L';
            break;
          case 1:
            *DISFINptr++ = 'I';
            *DISFINptr++ = 'X';
            break;
          case 2:
            *DISFINptr++ = 'I';
            *DISFINptr++ = 'Y';
            break;
        }
        break;
      case 'x':
        /* x means (HL) unless prefix says (IX+dd) or (IY+dd) */
        switch(INDEX)
        {
          case 0:
            *DISFINptr++ = '(';
            *DISFINptr++ = 'H';
            *DISFINptr++ = 'L';
            *DISFINptr++ = ')';
            break;
          case 1:
            if (DISP < 0)
            {
              DISP = getbyte ();
              ilen++;
            }
            if (DISP >= 128)
              sprintf (tmp, "(IX-%02x)", 256 - DISP);
            else
              sprintf (tmp, "(IX+%02x)", DISP);
            addtmp ();
            break;
          case 2:
            if (DISP < 0)
            {
              DISP = getbyte ();
              ilen++;
            }
            if (DISP >= 128)
              sprintf (tmp, "(IY-%02x)", 256 - DISP);
            else
              sprintf (tmp, "(IY+%02x)", DISP);
            addtmp ();
            break;
        }
        break;
      default:
        *DISFINptr++ = DIS[x];
    }
  }
  /* pad hex output to align instructions */
  if (opt_hex) for (x = 0; x < (5 - ilen) * 2; x++) putchar (' ');
  *DISFINptr = '\0';
  /* finally output the instruction */
  printf ("%s\n", DISFINAL);
}


/*
#############################################################################
#
#       Add 'tmp' to DISFINAL
#
#############################################################################
*/

void addtmp (void)
{
  *DISFINptr = '\0';
  strcat (DISFINAL, tmp);
  while (*DISFINptr++)
    /*nothing*/;
  DISFINptr--;
}


/*
#############################################################################
#
#       The guts of the decoder (level 0)
#
#       Z80 instruction set follows certain patterns due to the way the
#       processor decodes instructions, so we leverage that.
#
#       7 6 5 4 3 2 1 0
#       F F G G G H H H
#           J J K
#
#       CLASS is zero normally, 1 for CB prefix, 2 for ED prefix
#
#############################################################################
*/

void decodebyte (unsigned char byte)
{
  __F = (byte & 0xc0) >> 6;
  __G = (byte & 0x38) >> 3;
  __H = (byte & 0x07);
  __J = (__G & 0x06) >> 1;
  __K = (__G & 0x01);
  (*_C[CLASS]) (byte);
}


/*
#############################################################################
#
#       The guts of the decoder (level 1)
#
#       HALT decoded here as doesn't fit the pattern
#       (or maybe as the pattern suggests LD (HL),(HL) is causes a halt)
#       CB and ED prefixes set the instruction class and we get another byte
#       DD and FD prefixes signal indexed instructions (IX/IY)
#       Else decode based on top two bits
#
#############################################################################
*/

/* regular */
void _C0 (unsigned char byte)
{
  switch(byte)
  {
    case 0x76:
      strcpy (DIS, "HALT");
      return;
    case 0xCB:
      CLASS = 1;
      if (INDEX)
      {
        DISP = getbyte ();
        ilen++;
      }
      decodebyte (getbyte ());
      ilen++;
      return;
    case 0xED:
      CLASS = 2;
      decodebyte (getbyte ());
      ilen++;
      return;
    case 0xDD:
      INDEX = 1;
      decodebyte (getbyte ());
      ilen++;
      return;
    case 0xFD:
      INDEX = 2;
      decodebyte (getbyte ());
      ilen++;
      return;
  }
  (*_C0F[__F]) ();
}

/* CB prefix */
void _C1 (unsigned char byte)
{
  (*_C1F[__F]) ();
}

/* ED prefix */
void _C2 (unsigned char byte)
{
  (*_C2F[__F]) ();
}


/*
#############################################################################
#
#       The guts of the decoder (level 2)
#
#############################################################################
*/

/* 00 to 3F - mixed bag */
void _C0F0 (void)
{
  (*_C0F0H[__H]) ();
}

/* 40 to 7F are instruction to instruction loads, except 76 */
/* instruction bit fields 01GGGHHH determine registers */
void _C0F1 (void)
{
  strcpy (DIS, "LD   ");
  strcat (DIS, __r[__G]);
  strcat (DIS, ",");
  strcat (DIS, __r[__H]);
}

/* 80 to BF are 8 different math operations */
/* instruction bit fields 10GGGHHH determine operation and register */
/* ADD, ADC, SUB, SBC, AND, XOR, OR, CP */
/* CP (compare) is a subtract throwing away result */
void _C0F2 (void)
{
  strcpy (DIS, __x[__G]);
  strcat (DIS, __r[__H]);
}

/* C0 to FF - mixed bag */
void _C0F3 (void)
{
  (*_C0F3H[__H]) ();
}

/* CB then 00 to 3F are rotate/shift operations */
/* instruction bit field 00GGGHHH determine operation and register */
void _C1F0 (void)
{
  switch(__G)
  {
    case 0:
      strcpy (DIS, "RLC  ");
      break;
    case 1:
      strcpy (DIS, "RRC  ");
      break;
    case 2:
      strcpy (DIS, "RL   ");
      break;
    case 3:
      strcpy (DIS, "RR   ");
      break;
    case 4:
      strcpy (DIS, "SLA  ");
      break;
    case 5:
      strcpy (DIS, "SRA  ");
      break;
    case 6:
      strcpy (DIS, ""); /* undocumented SLL - buggy */
      break;
    case 7:
      strcpy (DIS, "SRL  ");
      break;
  }
  strcat (DIS, __r[__H]);
}

/* CB then 40 to 7F are bit test operations */
/* instruction bit field 01GGGHHH determine bit and register */
void _C1F1 (void)
{
  strcpy (DIS, "BIT  ");
  strcat (DIS, __n[__G]);
  strcat (DIS, ",");
  strcat (DIS, __r[__H]);
}

/* CB then 80 to BF are bit reset operations */
/* instruction bit field 01GGGHHH determine bit and register */
void _C1F2 (void)
{
  strcpy (DIS, "RES  ");
  strcat (DIS, __n[__G]);
  strcat (DIS, ",");
  strcat (DIS, __r[__H]);
}

/* CB then C0 to FF are bit set operations */
/* instruction bit field 01GGGHHH determine bit and register */
void _C1F3 (void)
{
  strcpy (DIS, "SET  ");
  strcat (DIS, __n[__G]);
  strcat (DIS, ",");
  strcat (DIS, __r[__H]);
}

/* ED then 40 to 7F - mixed bag */
void _C2F1 (void)
{
  (*_C2F1H[__H]) ();
}

/* ED then A0 to BF are the "block" operations */
/* instruction bitfield 10GGGHHH specify operation, direction, and repeat */
void _C2F2 (void)
{
  switch (__H)
  {
    case 0:
      strcpy (DIS, "LD");
      break;
    case 1:
      strcpy (DIS, "CP");
      break;
    case 2:
      strcpy (DIS, "IN");
      break;
    case 3:
      strcpy (DIS, "OT");
      break;
  }
  switch(__G)
  {
    case 4:
      strcat (DIS, "I");
      break;
    case 5:
      strcat (DIS, "D");
      break;
    case 6:
      strcat (DIS, "IR");
      break;
    case 7:
      strcat (DIS, "DR");
      break;
  }
}


/*
#############################################################################
#
#       The guts of the decoder (level 3)
#
#############################################################################
*/

/* 00/08/10/18/20/28/30/38 mixed bag, 5 of 8 are relative jumps */
/* 20/28/30/38 are conditional relative jumps */
void _C0F0H0 (void)
{
  if (__G > 3)
  {
    strcpy (DIS, "JR   ");
    strcat (DIS, __c[__G-4]);
    strcat (DIS, ",@v");
  }
  else
  {
    switch (__G)
    {
      case 0:
        strcpy (DIS, "NOP");
        break;
      case 1:
        strcpy (DIS, "EX   AF,AF'");
        break;
      case 2:
        strcpy (DIS, "DJNZ @v");
        break;
      case 3:
        strcpy (DIS, "JR   @v");
        break;
    }
  }
}

/* 01/09/11/19/21/29/31/39 - LD and ADD */
void _C0F0H1 (void)
{
  (*_C0F0H1K[__K]) ();
}

/* 02/0A/12/1A/22/2A/32/3A various load instructions */
/* instruction bitfield 00GGG010 specifies to and from */
void _C0F0H2 (void)
{
  strcpy (DIS, "LD   ");
  switch (__G)
  {
    case 0:
      strcat (DIS, "(BC),A");
      break;
    case 1:
      strcat (DIS, "A,(BC)");
      break;
    case 2:
      strcat (DIS, "(DE),A");
      break;
    case 3:
      strcat (DIS, "A,(DE)");
      break;
    case 4:
      strcat (DIS, "(vv),y");
      break;
    case 5:
      strcat (DIS, "y,(vv)");
      break;
    case 6:
      strcat (DIS, "(vv),A");
      break;
    case 7:
      strcat (DIS, "A,(vv)");
      break;
  }
}

/* 03/0B/13/1B/23/2B/33/3B - INC and DEC */
void _C0F0H3 (void)
{
  (*_C0F0H3K[__K]) ();
}

/* 04/0C/14/1C/24/2C/34/3C - register INC */
/* instruction bitfield 00GGG100 specifies register */
void _C0F0H4 (void)
{
  strcpy (DIS, "INC  ");
  strcat (DIS, __r[__G]);
}

/* 05/0D/15/1D/25/2D/35/3D - register DEC */
/* instruction bitfield 00GGG101 specifies register */
void _C0F0H5 (void)
{
  strcpy (DIS, "DEC  ");
  strcat (DIS, __r[__G]);
}

/* 06/0E/16/1E/26/2E/36/3E - register LD */
/* instruction bitfield 00GGG110 specifies register */
void _C0F0H6 (void)
{
  strcpy (DIS, "LD   ");
  strcat (DIS, __r[__G]);
  strcat (DIS, ",v");
}

/* 07/0F/17/1F/27/2F/37/3F - 4 rotates, 4 others */
void _C0F0H7 (void)
{
  switch (__G)
  {
    case 0:
      strcpy (DIS, "RLCA");
      break;
    case 1:
      strcpy (DIS, "RRCA");
      break;
    case 2:
      strcpy (DIS, "RLA");
      break;
    case 3:
      strcpy (DIS, "RRA");
      break;
    case 4:
      strcpy (DIS, "DAA");
      break;
    case 5:
      strcpy (DIS, "CPL");
      break;
    case 6:
      strcpy (DIS, "SCF");
      break;
    case 7:
      strcpy (DIS, "CCF");
      break;
  }
}

/* C0/C8/D0/D8/E0/E8/F0/F8 - conditional return */
/* instruction bitfield 11GGG000 specifies condition */
void _C0F3H0 (void)
{
  strcpy (DIS, "RET  ");
  strcat (DIS, __c[__G]);
}

/* C1/C9/D1/D9/E1/E9/F1/F9 */
void _C0F3H1 (void)
{
  (*_C0F3H1K[__K]) ();
}

/* C2/CA/D2/DA/E2/EA/F2/FA - conditional jumps */
/* instruction bitfield 11GGG010 specifies condition */
void _C0F3H2 (void)
{
  strcpy (DIS, "JP   ");
  strcat (DIS, __c[__G]);
  strcat (DIS, ",vv");
}

/* C3/CB/D3/DB/E3/EB/F3/FB */
void _C0F3H3(void)
{
  switch (__G)
  {
    case 0:
      strcpy (DIS, "JP   vv");
      break;
    case 1:
      strcpy (DIS, ""); /* CB prefix */
      break;
    case 2:
      strcpy (DIS, "OUT  (v),A");
      break;
    case 3:
      strcpy (DIS, "IN   A,(v)");
      break;
    case 4:
      strcpy (DIS, "EX   (SP),y");
      break;
    case 5:
      strcpy (DIS, "EX   DE,HL");
      break;
    case 6:
      strcpy (DIS, "DI");
      break;
    case 7:
      strcpy (DIS, "EI");
      break;
  }
}

/* C4/CC/D4/DC/E4/EC/F4/FC - conditional calls */
/* instruction bitfield 11GGG100 specifies condition */
void _C0F3H4 (void)
{
  strcpy (DIS, "CALL ");
  strcat (DIS, __c[__G]);
  strcat (DIS, ",vv");
}

/* C5/CD/D5/DD/E5/ED/F5/FD - PUSH instruction, CALL, and prefixes */
void _C0F3H5 (void)
{
  (*_C0F3H5K[__K]) ();
}

/* C6/CE/D6/DE/E6/EE/F6/FE - math operations on accumulator */
/* instruction bit fields 11GGG110 determine operation */
/* ADD, ADC, SUB, SBC, AND, XOR, OR, CP */
/* CP (compare) is a subtract throwing away result */
void _C0F3H6 (void)
{
  strcpy (DIS, __x[__G]);
  strcat (DIS, "v");
}

/* C7/CF/D7/DF/E7/EF/F7/FF */
/* the "restart" instructions */
/* we could calculate these based on 11GGG111 - G << 3 */
void _C0F3H7( void)
{
  strcpy (DIS, "RST  ");
  switch (__G)
  {
    case 0:
      strcat (DIS, "00");
      break;
    case 1:
      strcat (DIS, "08");
      break;
    case 2:
      strcat (DIS, "10");
      break;
    case 3:
      strcat (DIS, "18");
      break;
    case 4:
      strcat (DIS, "20");
      break;
    case 5:
      strcat (DIS, "28");
      break;
    case 6:
      strcat (DIS, "30");
      break;
    case 7:
      strcat (DIS, "38");
      break;
  }
}

/* ED then 40/48/50/58/60/68/70/78 - IN (C) instructions */
/* instruction bitfield 01GGG000 specified register */
void _C2F1H0 (void)
{
  strcpy (DIS, "IN   ");
  strcat (DIS, __r[__G]);
  strcat (DIS, ",(C)");
}

/* ED then 41/49/51/59/61/69/71/79 - OUT (C) instructions */
void _C2F1H1 (void)
{
  strcpy (DIS, "OUT  (C),");
  strcat (DIS, __r[__G]);
}

/* ED then 42/4A/52/5A/62/6A/72/7A - 16-bit SBC and ADC from HL */
void _C2F1H2 (void)
{
  (*_C2F1H2K[__K]) ();
}

/* ED then 43/4B/53/5B/63/6B/73/7B - 16 bit LD from memory */
void _C2F1H3 (void)
{
  (*_C2F1H3K[__K]) ();
}

/* ED 44 (others undocumented) */
void _C2F1H4 (void)
{
  strcpy (DIS, "NEG");
}

/* ED then 45/4D/55/5D/65/6D/75/7D */
void _C2F1H5 (void)
{
  (*_C2F1H5K[__K]) ();
}

/* ED then 46/4E/56/5E/66/6E/76/7E - set interrupt mode */
void _C2F1H6 (void)
{
  switch (__G)
  {
    case 0:
      strcpy (DIS, "IM   0");
      break;
    case 1:
      strcpy (DIS, "");
      break;
    case 2:
      strcpy (DIS, "IM   1");
      break;
    case 3:
      strcpy (DIS, "IM   2");
      break;
    case 4:
      strcpy (DIS, "");
      break;
    case 5:
      strcpy (DIS, "");
      break;
    case 6:
      strcpy (DIS, "");
      break;
    case 7:
      strcpy (DIS, "");
      break;
  }
}

/* ED then 47/4F/57/5F/67/6F/77/7F */
void _C2F1H7 (void)
{
  switch (__G)
  {
    case 0:
      strcpy (DIS, "LD   I,A");
      break;
    case 1:
      strcpy (DIS, "LD   R,A");
      break;
    case 2:
      strcpy (DIS, "LD   A,I");
      break;
    case 3:
      strcpy (DIS, "LD   A,R");
      break;
    case 4:
      strcpy (DIS, "RRD");
      break;
    case 5:
      strcpy (DIS, "RLD");
      break;
    case 6:
      strcpy (DIS, "");
      break;
    case 7:
      strcpy (DIS, "");
      break;
  }
}


/*
#############################################################################
#
#       The guts of the decoder (level 4)
#
#       This is where we break down xxGGGxxx a lot
#       Recap: GGG broken down into JJK
#
#############################################################################
*/

/* 01/11/21/31 - 16 bit LD */
/* instruction bitfield 00JJ0001 specifies register pair */
void _C0F0H1K0 (void)
{
  strcpy (DIS, "LD   ");
  strcat (DIS, __s[__J]);
  strcat (DIS, ",vv");
}

/* 09/19/29/39 - 16 bit ADD to HL */
/* instruction bitfield 00JJ1001 specifies register pair */
void _C0F0H1K1 (void)
{
  strcpy (DIS, "ADD  y,");
  strcat (DIS, __s[__J]);
}

/* 03/13/23/33 - 16 bit increment */
/* instruction bitfield 00JJ0011 specifies register pair */
void _C0F0H3K0 (void)
{
  strcpy (DIS, "INC  ");
  strcat (DIS, __s[__J]);
}

/* 0B/1B/2B/3B - 16 bit decrement */
/* instruction bitfield 00JJ1011 specifies register pair */
void _C0F0H3K1 (void)
{
  strcpy (DIS, "DEC  ");
  strcat (DIS, __s[__J]);
}

/* C1/D1/E1/F1 - POP instructions */
/* instruction bitfield 11JJ0001 specifies register pair */
void _C0F3H1K0 (void)
{
  strcpy (DIS, "POP  ");
  strcat (DIS, __q[__J]);
}

/* C9/D9/E9/F9 */
void _C0F3H1K1 (void)
{
  switch (__J)
  {
    case 0:
      strcpy (DIS, "RET");
      break;
    case 1:
      strcpy (DIS, "EXX");
      break;
    case 2:
      strcpy (DIS, "JP   (y)");
      break;
    case 3:
      strcpy (DIS, "LD   SP,y");
      break;
  }
}

/* C5/D5/E5/F5 - POP instructions */
/* instruction bitfield 11JJ0101 specifies register pair */
void _C0F3H5K0 (void)
{
  strcpy (DIS, "PUSH ");
  strcat (DIS, __q[__J]);
}

/* CD - CALL instruction (DD/ED/FD are prefixes) */
void _C0F3H5K1 (void)
{
  strcpy (DIS, "CALL vv");
}

/* ED then 42/52/62/72 - 16-bit subtract with carry from HL */
/* instruction bitfield 01JJ0010 specifies register pair */
void _C2F1H2K0 (void)
{
  strcpy (DIS, "SBC  HL,");
  strcat (DIS, __s[__J]);
}

/* ED then 4A/5A/6A/7A - 16-bit add with carry to HL */
/* instruction bitfield 01JJ1010 specifies register pair */
void _C2F1H2K1 (void)
{
  strcpy (DIS, "ADC  HL,");
  strcat (DIS, __s[__J]);
}

/* ED then 43/53/63/73 - 16-bit load to memory */
/* instruction bitfield 01JJ0011 specifies source register pair */
void _C2F1H3K0 (void)
{
  strcpy (DIS, "LD   (vv),");
  strcat (DIS, __s[__J]);
}

/* ED then 4B/5B/6B/7B - 16-bit load from memory */
/* instruction bitfield 01JJ1011 specifies destination register pair */
void _C2F1H3K1 (void)
{
  strcpy (DIS, "LD   ");
  strcat (DIS, __s[__J]);
  strcat (DIS, ",(vv)");
}

/* 45 - RETN (55/65/75 undocumented) */
void _C2F1H5K0 (void)
{
  strcpy (DIS, "RETN");
}

/* 4D - RETI (5D/6D/7D undocumented) */
void _C2F1H5K1 (void)
{
  strcpy (DIS, "RETI");
}


/*
#############################################################################
#
#       This replaces a couple of empty functions
#
#############################################################################
*/

void _Z80_EMPTY (void)
{
}


/*
#############################################################################
#
#       .end.
#
#############################################################################
*/
